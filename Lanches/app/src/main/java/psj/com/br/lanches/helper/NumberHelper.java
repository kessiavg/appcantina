package psj.com.br.lanches.helper;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by kessia on 17/08/2017.
 */

public class NumberHelper {


    public static final String LANGUAGE = "pt";
    public static final String COUNTRY = "BR";
    public static final String PATTERN = "0.##";

    public static String converterDoubleParaStringMonetario(float valorFloat){
        Locale ptBr = new Locale(LANGUAGE, COUNTRY);
        return NumberFormat.getCurrencyInstance(ptBr).format(valorFloat);
    }

    public static String converterDoubleParaStringMonetario(Double valorDouble){
        Locale ptBr = new Locale(LANGUAGE, COUNTRY);
        return NumberFormat.getCurrencyInstance(ptBr).format(valorDouble.doubleValue());
    }

    public static String converterStringDuasCasasDecimais(float valorFloat){
        DecimalFormat df = new DecimalFormat(PATTERN);
        return df.format(valorFloat);
    }

}
