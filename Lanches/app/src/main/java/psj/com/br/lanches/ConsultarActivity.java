package psj.com.br.lanches;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import psj.com.br.lanches.model.Cantina;

public class ConsultarActivity extends AuxActivity {

    ListView listView;
    ArrayList<Cantina> cantinas = new ArrayList<>();
    TextView textViewEmptyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cantina c = getCantinas().get((int) id);
                abrirTelaListaLancamentos(c);
            }
        });

        textViewEmptyMessage = (TextView) findViewById(R.id.emptyMessageCantinas);
        textViewEmptyMessage.setVisibility(View.INVISIBLE);

        DatabaseReference raiz = FirebaseDatabase.getInstance().getReference(); //Esta variavel indica a raiz da nossa árvore JSON
        Query query = raiz.child("cantina").orderByChild("dataTime");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator<DataSnapshot> list = dataSnapshot.getChildren().iterator();

                while (list.hasNext()){
                    DataSnapshot item = list.next();
                    String depto = (String) item.child("departamento").getValue();
                    String lanche = (String) item.child("lanche").getValue();
                    HashMap data = (HashMap) item.child("date").getValue();
                    Object totalPago = item.child("totalPago").getValue();
                    Boolean cantinaEncerrada = (Boolean) item.child("cantinaEncerrada").getValue();

                    Double totalPagoDouble = new Double(0.0);

                    if (totalPago instanceof Double){
                        totalPagoDouble = (Double) totalPago;
                    } else if (totalPago instanceof Long){
                        totalPagoDouble = Double.valueOf((Long)totalPago);
                    }

                    Calendar cal = Calendar.getInstance();
                    Long diaMes = (Long) data.get("date");
                    Long mes = (Long) data.get("month");
                    String anoString = data.get("year").toString().substring(1,3);

                    cal.set(Calendar.DAY_OF_MONTH, diaMes.intValue());
                    cal.set(Calendar.MONTH, mes.intValue());
                    cal.set(Calendar.YEAR, Integer.parseInt(anoString));

                    Cantina cantina = new Cantina(item.getKey(), cal.getTime(), lanche, depto);
                    cantina.setTotalPago(totalPagoDouble);
                    cantina.setCantinaEncerrada(cantinaEncerrada.booleanValue());
                    getCantinas().add(cantina);

                }
                setarCantinasRetornadas();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ConsultarActivity.this, "Erro inesperado.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setarCantinasRetornadas(){

        if (getCantinas().isEmpty()){
            textViewEmptyMessage.setVisibility(View.VISIBLE);
        }
        CantinaListAdapter adapter = new CantinaListAdapter(this, R.layout.adapter_view_layout, getCantinas());
        listView.setAdapter(adapter);
    }

    private void abrirTelaListaLancamentos(Cantina c) {
        Intent intent  = new Intent(ConsultarActivity.this, VisualizarLancamentosActivity.class);
        VisualizarLancamentosActivity.setarCantinaSession(c);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {
        case R.id.iniciar:
            abrirTelaInserir(ConsultarActivity.this);
            return true;
        case R.id.consultar:
            return true;
        case R.id.relatorio:
            abrirTelaRelatorio(ConsultarActivity.this);
            return true;
        case R.id.sair:
            finish();
            System.exit(0);
            return true;
    }
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<Cantina> getCantinas() {
        return cantinas;
    }

    public void setCantinas(ArrayList<Cantina> cantinas) {
        this.cantinas = cantinas;
    }
}
