package psj.com.br.lanches;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import psj.com.br.lanches.dao.ConfiguracaoFirebase;
import psj.com.br.lanches.model.Cantina;
import psj.com.br.lanches.model.Lancamento;

public class RelatorioActivity extends AuxActivity {

    private EditText edtNomeCliente;
    private Button btnBuscarRelatorio;
    ArrayList<Lancamento> lancamentos = new ArrayList<>();
    ListView listView;
    DatabaseReference referenciaFirebase = ConfiguracaoFirebase.getFirebase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio);

        edtNomeCliente = (EditText) findViewById(R.id.edtNomeClienteConsulta);
        btnBuscarRelatorio = (Button) findViewById(R.id.buscarRelatorio);

        listView = (ListView) findViewById(R.id.listViewRelatorio);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Lancamento l = getLancamentos().get((int) id);
                chamarConfirmacao(l);
            }
        });

        btnBuscarRelatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomeClienteFiltro = edtNomeCliente.getText().toString();
                buscarRelatorioPorClienteChild(nomeClienteFiltro);
            }
        });
    }

    public void chamarConfirmacao(final Lancamento l) {

        if (l.getPago()) {
            Toast.makeText(this, "Já está pago.", Toast.LENGTH_SHORT).show();
        } else {

            AlertDialog alert = new AlertDialog.Builder(RelatorioActivity.this)
                    .setTitle("Pagar")
                    .setMessage("Confirma Pagamento?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            atualizarPagamento(l);
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }
    }

    private void atualizarPagamento(Lancamento lanc) {

        lanc.setPago(true);
        referenciaFirebase.child("lancamento").child(lanc.getIdLancamento()).setValue(lanc);

        try {
            Cantina c = (Cantina) lanc.getCantina().clone();
            if (lanc.getCantina().getTotalPago() != null) {
                c.setTotalPago(lanc.getCantina().getTotalPago() + lanc.getValor());
            } else {
                c.setTotalPago(lanc.getValor());
            }
            lanc.getCantina().setTotalPago(c.getTotalPago());
            referenciaFirebase.child("cantina").child(lanc.getCantina().getKey()).setValue(c);
        } catch (Exception e){

        }
    }

    public void buscarRelatorioPorClienteChild(String nomeCliente) {
        Query query = referenciaFirebase.child("lancamento").orderByChild("nomeCliente").equalTo(nomeCliente);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String idCantina = (String) dataSnapshot.child("idCantina").getValue();
                String nomeCliente = (String) dataSnapshot.child("nomeCliente").getValue();
                Object valor = dataSnapshot.child("valor").getValue();
                Boolean pago = (Boolean) dataSnapshot.child("pago").getValue();
                String idLancamento = (String) dataSnapshot.child("idLancamento").getValue();

                Double valorDouble = new Double(0.0);

                if (valor instanceof Double){
                    valorDouble = (Double) valor;

                } else if (valor instanceof Long){
                    valorDouble = Double.valueOf((Long)valor);
                }

                final Lancamento l = new Lancamento(idCantina, valorDouble, pago, nomeCliente);
                l.setIdLancamento(idLancamento);

                referenciaFirebase.child("cantina/" + idCantina).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {

                        Cantina c = new Cantina();
                        HashMap t = (HashMap) snapshot.getValue();
                        String depto = (String) t.get("departamento");
                        String lanche = (String) t.get("lanche");

                        HashMap data = (HashMap) t.get("date");
                        Calendar cal = Calendar.getInstance();
                        Long diaMes = (Long) data.get("date");
                        Long mes = (Long) data.get("month");
                        String anoString = data.get("year").toString().substring(1, 3);

                        cal.set(Calendar.DAY_OF_MONTH, diaMes.intValue());
                        cal.set(Calendar.MONTH, mes.intValue() + 1);
                        cal.set(Calendar.YEAR, Integer.parseInt(anoString));

                        c.setDate(cal.getTime());
                        c.setLanche(lanche);
                        c.setDepartamento(depto);

                        l.setCantina(c);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                if (getLancamentos().isEmpty()){
                    getLancamentos().add(l);
                } else {
                    for (Lancamento lanc : getLancamentos()) {
                        if (!l.getIdLancamento().equals(lanc.getIdLancamento())) {
                            getLancamentos().add(l);
                            break;
                        }
                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        setarLancamentos();
    }

    private void setarLancamentos() {

        final RelatorioClienteListAdapter adapter = new RelatorioClienteListAdapter(this,
                R.layout.adapter_view_layout_rel, getLancamentos());

        listView.setAdapter(adapter);
    }

    public ArrayList<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(ArrayList<Lancamento> lancamentos) {
        this.lancamentos = lancamentos;
    }
}
