package psj.com.br.lanches;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import psj.com.br.lanches.helper.DateHelper;
import psj.com.br.lanches.helper.NumberHelper;
import psj.com.br.lanches.model.Cantina;
import psj.com.br.lanches.model.Lancamento;

/**
 * Created by kessia on 10/08/2017.
 */

public class RelatorioClienteListAdapter extends ArrayAdapter<Lancamento> {
    private Context context;
    int resource;

    public RelatorioClienteListAdapter(Context context, int resource, ArrayList<Lancamento> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String nome = getItem(position).getNomeCliente();
        Double valor = getItem(position).getValor();
        Boolean pago = getItem(position).getPago();
        Cantina c = getItem(position).getCantina();

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource, parent, false);

        //criando campos de texto
        TextView tvNome = (TextView) convertView.findViewById(R.id.textViewNomeRel);
        TextView tvValor = (TextView) convertView.findViewById(R.id.textViewValorRel);
        TextView tvPago = (TextView) convertView.findViewById(R.id.textViewPagoRel);
        TextView tvData = (TextView) convertView.findViewById(R.id.textViewDataRel);
        TextView tvLanche = (TextView) convertView.findViewById(R.id.textViewLancheRel);

        tvNome.setText(nome);
        tvValor.setText(NumberHelper.converterDoubleParaStringMonetario(valor));

        if (c.getDate() != null) {
            tvData.setText(DateHelper.getDataFormatada(c.getDate()));
        }
        tvLanche.setText(c.getLanche());

        if (pago){
            tvPago.setText("Pago");
            tvPago.setTextColor(0xFF119C2C);
            tvValor.setTextColor(0xFF119C2C);
        } else {
            tvPago.setText("Pendente");
            tvPago.setTextColor(Color.RED);
            tvValor.setTextColor(Color.RED);
        }

        return convertView;
    }

}
