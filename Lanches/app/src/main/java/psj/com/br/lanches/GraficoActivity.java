package psj.com.br.lanches;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

import psj.com.br.lanches.helper.NumberHelper;

public class GraficoActivity extends AppCompatActivity {

    private static float percentPago;
    private static float percentPendente;
    private static float valorPago;
    private static float valorPendente;

    private String [] xData = {"Pago", "Pendente"};
    PieChart pieChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico);

        final float[] arrayValores = {valorPago, valorPendente};
        final Float[] yData = {percentPago, percentPendente};

        pieChart = (PieChart) findViewById(R.id.graficoVendas);

        Description d = new Description();
        d.setText("");
        pieChart.setDescription(d);
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setDrawEntryLabels(true);

        addDataSet(yData);

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

                int pos1 = e.toString().indexOf("y: ");
                String sales = e.toString().substring(pos1 + 3);

                for (int i = 0; i < yData.length; i++){
                    if (yData[i].equals(Float.parseFloat(sales))){
                        pos1 = i;
                        break;
                    }
                }

                String valorString = NumberHelper.converterDoubleParaStringMonetario(arrayValores[pos1]);
                String percentString = NumberHelper.converterStringDuasCasasDecimais(yData[pos1]);
                String status = xData[pos1];

                Toast.makeText(GraficoActivity.this, status + ": " + percentString + "% - " + valorString, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });

    }


    private void addDataSet(Float[] yData) {
        ArrayList<PieEntry> yEntries = new ArrayList<>();
        ArrayList<String> xEntries = new ArrayList<>();

        for (int i = 0; i < yData.length; i++){
            yEntries.add(new PieEntry(yData[i], xData[i], i));
        }
        for (int i = 0; i < xData.length; i++){
            xEntries.add(xData[i]);
        }

        //PieDataSet
        //PieDataSet p = new PieDataSet(yEntries, xEntries, )
        PieDataSet pieDataSet = new PieDataSet(yEntries, "Vendas:");
        pieDataSet.setSliceSpace(1);
        pieDataSet.setValueTextSize(13);

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.RED);

        pieDataSet.setColors(colors);

        Legend legend  = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.DEFAULT);
        legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legend.setEnabled(false);

        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.invalidate();
    }


    public static float getPercentPago() {
        return percentPago;
    }

    public static void setPercentPago(float percentPago) {
        GraficoActivity.percentPago = percentPago;
    }

    public static float getPercentPendente() {
        return percentPendente;
    }

    public static void setPercentPendente(float percentPendente) {
        GraficoActivity.percentPendente = percentPendente;
    }

    public static float getValorPago() {
        return valorPago;
    }

    public static void setValorPago(float valorPago) {
        GraficoActivity.valorPago = valorPago;
    }

    public static float getValorPendente() {
        return valorPendente;
    }

    public static void setValorPendente(float valorPendente) {
        GraficoActivity.valorPendente = valorPendente;
    }

}
