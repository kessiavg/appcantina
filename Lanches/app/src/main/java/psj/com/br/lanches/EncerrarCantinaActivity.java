package psj.com.br.lanches;

import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;

import psj.com.br.lanches.dao.ConfiguracaoFirebase;
import psj.com.br.lanches.helper.Mask;
import psj.com.br.lanches.model.Cantina;

public class EncerrarCantinaActivity extends AuxActivity {

    private static Cantina cantinaSession;
    EditText edtValorRecebidoEspecie;
    Button btnEncerrar;
    Button btnCancelar;
    DatabaseReference referenciaFirebase = ConfiguracaoFirebase.getFirebase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encerrar_cantina);

        edtValorRecebidoEspecie = (EditText) findViewById(R.id.edtValorRecebido);
        TextWatcher valorMask = Mask.monetario(edtValorRecebidoEspecie);
        edtValorRecebidoEspecie.addTextChangedListener(valorMask);

        btnCancelar = (Button) findViewById(R.id.btnCancelarFechamento);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnEncerrar = (Button) findViewById(R.id.btnEncerrar);
        btnEncerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valorRecebidoStr = edtValorRecebidoEspecie.getText().toString();
                valorRecebidoStr = valorRecebidoStr.replace(",", ".");
                atualizarValorCantina(Double.parseDouble(valorRecebidoStr));
                abrirTelaConsultar(EncerrarCantinaActivity.this);
            }
        });
    }

    private void atualizarValorCantina(Double valorRecebido) {
        try {
            Cantina c = (Cantina) cantinaSession.clone();
            if (cantinaSession.getTotalPago() != null) {
                c.setTotalPago(cantinaSession.getTotalPago() + valorRecebido);
            } else {
                c.setTotalPago(valorRecebido);
            }
            c.setCantinaEncerrada(true);
            referenciaFirebase.child("cantina").child(cantinaSession.getKey()).setValue(c);
        } catch (Exception e){

        }
    }

    public static void setarHash(Cantina c){
        cantinaSession = c;
    }
}
