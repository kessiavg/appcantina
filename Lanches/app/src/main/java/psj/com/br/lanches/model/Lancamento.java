package psj.com.br.lanches.model;

/**
 * Created by kessia on 09/08/2017.
 */

public class Lancamento {
    private String idCantina;
    private Double valor;
    private Boolean pago;
    private String nomeCliente;
    private String idLancamento;
    private Cantina cantina;

    public Lancamento(){

    }

    public Lancamento(String idCantina, Double valor, Boolean pago, String nomeCliente) {
        this.idCantina = idCantina;
        this.valor = valor;
        this.pago = pago;
        this.nomeCliente = nomeCliente;
    }

    public String getNomeCliente() {

        return nomeCliente;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getIdCantina() {
        return idCantina;
    }

    public void setIdCantina(String idCantina) {
        this.idCantina = idCantina;
    }

    public Boolean getPago() {
        return pago;
    }

    public void setPago(Boolean pago) {
        this.pago = pago;
    }


    public Cantina getCantina() {
        return cantina;
    }

    public void setCantina(Cantina cantina) {
        this.cantina = cantina;
    }

    public String getIdLancamento() {
        return idLancamento;
    }

    public void setIdLancamento(String idLancamento) {
        this.idLancamento = idLancamento;
    }

}
