package psj.com.br.lanches;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import psj.com.br.lanches.dao.ConfiguracaoFirebase;
import psj.com.br.lanches.helper.DateHelper;
import psj.com.br.lanches.helper.Mask;
import psj.com.br.lanches.model.Cantina;

public class InserirActivity extends AuxActivity implements AdapterView.OnItemSelectedListener{

    String[] deptoNames={"Geral", "Adolescentes", "Cantata", "Círculo de Oração", "Coral", "Crianças",
            "Jovens", "Reforma","Uniforme"};

    private EditText edtData;
    private EditText edtLanche;
    private EditText edtPreco;
    private EditText edtValorInicial;
    private Button btnConfirmar;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private String deptoSelecionado;
    DatabaseReference referenciaFirebase = ConfiguracaoFirebase.getFirebase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserir);

        edtData = (EditText) findViewById(R.id.edtData);

        Date d = new Date();

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String date = (month + 1) + "/" + day + "/" + year;
        edtData.setText(date);

        edtLanche = (EditText) findViewById(R.id.edtLanche);

        edtPreco = (EditText) findViewById(R.id.edtPreco);
        TextWatcher precoMask = Mask.monetario(edtPreco);
        edtPreco.addTextChangedListener(precoMask);

        edtValorInicial = (EditText) findViewById(R.id.edtValorInicial);
        TextWatcher valorMask = Mask.monetario(edtValorInicial);
        edtValorInicial.addTextChangedListener(valorMask);

        btnConfirmar = (Button) findViewById(R.id.btnConfirmar);

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gravar(edtData.getText().toString(),
                        edtLanche.getText().toString(),
                        edtPreco.getText().toString(),
                        edtValorInicial.getText().toString());
            }
        });

        edtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        InserirActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateSetListener, year, month, day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener(){
            public void onDateSet(DatePicker datePicker, int year, int month, int day){
                month = month + 1;
                String date = month + "/" + day + "/" + year;
                edtData.setText(date);
            }
        };

        Spinner spin = (Spinner) findViewById(R.id.simpleSpinner);
        spin.setOnItemSelectedListener(this);

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,deptoNames);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);
    }
    //Performing action onItemSelected and onNothing selected

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,long id) {
        setDeptoSelecionado(deptoNames[position]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
// TODO Auto-generated method stub

    }

    private void gravar(String data, String lanche, String preco, String valorInicial) {

        String valorInicialFormatado = valorInicial.replace(",", ".");
        String precoFormatado = preco.replace(",", ".");

        DatabaseReference ref = referenciaFirebase.child("cantina").push();

        Cantina c = new Cantina(ref.getKey(), DateHelper.convertStringToDate(data), lanche, getDeptoSelecionado()); //instancia do novo utilizador
        c.setDataTime(DateHelper.convertStringToDate(data).getTime());
        c.setValorInicial(Double.parseDouble(valorInicialFormatado));
        c.setPrecoUnitario(Double.parseDouble(precoFormatado));
        c.setCantinaEncerrada(false);

        ref.setValue(c);
        Toast.makeText(InserirActivity.this, "Cantina iniciada com sucesso.", Toast.LENGTH_LONG).show();

        VisualizarLancamentosActivity.setarCantinaSession(c);
        abrirTelaVisualizarLancamentos(InserirActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {
        case R.id.iniciar:
            return true;
        case R.id.consultar:
            abrirTelaConsultar(InserirActivity.this);
            return true;
        case R.id.relatorio:
            abrirTelaRelatorio(InserirActivity.this);
            return true;
        case R.id.sair:
            finish();
            return true;

    }
        return super.onOptionsItemSelected(item);
    }

    public String getDeptoSelecionado() {
        return deptoSelecionado;
    }

    public void setDeptoSelecionado(String deptoSelecionado) {
        this.deptoSelecionado = deptoSelecionado;
    }
}
