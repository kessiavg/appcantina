package psj.com.br.lanches.model;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by kessia on 09/08/2017.
 */

public class Cantina implements Cloneable{

    private Long codigo;
    private Date date;
    private Long dataTime;
    private Double valorInicial;
    private String lanche;
    private String departamento;
    private Double precoUnitario;
    private Double totalPago;
    private Double totalPendente;
    private String key;
    private boolean cantinaEncerrada;

    public Long getDataTime() {
        return dataTime;
    }

    public void setDataTime(Long dataTime) {
        this.dataTime = dataTime;
    }

    public Cantina() {
    }

    public Cantina(String key, Date date, String lanche, String departamento) {
        this.key = key;
        this.date = date;
        this.lanche = lanche;
        this.departamento = departamento;
    }

    public boolean isCantinaEncerrada() {
        return cantinaEncerrada;
    }

    public void setCantinaEncerrada(boolean cantinaEncerrada) {
        this.cantinaEncerrada = cantinaEncerrada;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }

    public String getLanche() {
        return lanche;
    }

    public void setLanche(String lanche) {
        this.lanche = lanche;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Double getTotalPago() {
        return totalPago;
    }

    public void setTotalPago(Double totalPago) {
        this.totalPago = totalPago;
    }

    public Double getTotalPendente() {
        return totalPendente;
    }

    public void setTotalPendente(Double totalPendente) {
        this.totalPendente = totalPendente;
    }

    public Double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(Double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
