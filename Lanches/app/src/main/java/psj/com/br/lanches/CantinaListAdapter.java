package psj.com.br.lanches;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import psj.com.br.lanches.helper.DateHelper;
import psj.com.br.lanches.model.Cantina;

/**
 * Created by kessia on 09/08/2017.
 */

public class CantinaListAdapter extends ArrayAdapter<Cantina> {

    private Context context;
    int resource;

    public CantinaListAdapter(Context context, int resource, ArrayList<Cantina> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Date date = getItem(position).getDate();
        String lanche = getItem(position).getLanche();
        String departamento = getItem(position).getDepartamento();
        Boolean encerrada = getItem(position).isCantinaEncerrada();

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource, parent, false);

        TextView tvDate = (TextView) convertView.findViewById(R.id.textView1);
        TextView tvLanche = (TextView) convertView.findViewById(R.id.textView2);
        TextView tvDepto = (TextView) convertView.findViewById(R.id.textView3);

        if (encerrada) {
            tvDate.setTextColor(Color.BLUE);
            tvLanche.setTextColor(Color.BLUE);
            tvDepto.setTextColor(Color.BLUE);
        }

        tvDate.setText(DateHelper.getDataFormatada(date));
        tvLanche.setText(lanche);
        tvDepto.setText(departamento);

        return convertView;
    }
}
