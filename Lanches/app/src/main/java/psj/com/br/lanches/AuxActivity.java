package psj.com.br.lanches;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;

import psj.com.br.lanches.ConsultarActivity;
import psj.com.br.lanches.EncerrarCantinaActivity;
import psj.com.br.lanches.GraficoActivity;
import psj.com.br.lanches.InserirActivity;
import psj.com.br.lanches.InserirLancamentoActivity;
import psj.com.br.lanches.RelatorioActivity;
import psj.com.br.lanches.VisualizarLancamentosActivity;
import psj.com.br.lanches.dao.ConfiguracaoFirebase;
import psj.com.br.lanches.model.Cantina;

public class AuxActivity extends AppCompatActivity {

    protected DatabaseReference referenciaFirebase = ConfiguracaoFirebase.getFirebase();

    protected void abrirTelaInserir(Context acOrigem) {
        startActivity(obterIntentTela(acOrigem, InserirActivity.class));
    }

    protected void abrirTelaConsultar(Context acOrigem) {
        startActivity(obterIntentTela(acOrigem, ConsultarActivity.class));
    }

    protected void abrirTelaRelatorio(Context acOrigem) {
        startActivity(obterIntentTela(acOrigem, RelatorioActivity.class));
    }

    protected void abrirTelaVisualizarLancamentos(Context acOrigem) {
        startActivity(obterIntentTela(acOrigem, VisualizarLancamentosActivity.class));
    }

    protected void abrirTelaInserirLancamentos(Context acOrigem, boolean modeInserir){
        InserirLancamentoActivity inserir = new InserirLancamentoActivity();
        inserir.modeInserir = modeInserir;

        startActivity(obterIntentTela(acOrigem, inserir.getClass()));
    }

    protected void abrirTelaInserirLancamentosEdicao(Context acOrigem, boolean modeInserir, String nome,
                                                     Double valor, String idLancamento, Cantina cantina){
        InserirLancamentoActivity inserir = new InserirLancamentoActivity();
        inserir.modeInserir = modeInserir;
        inserir.nome = nome;
        inserir.valor = valor;
        inserir.idLancamento = idLancamento;
        inserir.setarHash(cantina);

        startActivity(obterIntentTela(acOrigem, inserir.getClass()));
    }

    protected void abrirTelaGrafico(Context acOrigem){
        startActivity(obterIntentTela(acOrigem, GraficoActivity.class));
    }

    protected void abrirTelaEncerrar(Context acOrigem){
        startActivity(obterIntentTela(acOrigem, EncerrarCantinaActivity.class));
    }

    private Intent obterIntentTela(Context acOrigem, Class acDestino) {
        return new Intent(acOrigem, acDestino);
    }
}
