package psj.com.br.lanches;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AuxActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botaoInserir = (Button) findViewById(R.id.btnInserir);
        Button botaoConsultar = (Button) findViewById(R.id.btnConsultar);
        Button botaoRelatorio = (Button) findViewById(R.id.btnRelatorio);

        botaoInserir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaInserir(MainActivity.this);
            }
        });
        botaoConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaConsultar(MainActivity.this);
            }
        });
        botaoRelatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaRelatorio(MainActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {
        case R.id.iniciar:
            abrirTelaInserir(MainActivity.this);
            return true;
        case R.id.consultar:
            abrirTelaConsultar(MainActivity.this);
            return true;
        case R.id.relatorio:
            abrirTelaRelatorio(MainActivity.this);
            return true;
        case R.id.sair:
            finish();
            return true;
    }
        return super.onOptionsItemSelected(item);
    }
}