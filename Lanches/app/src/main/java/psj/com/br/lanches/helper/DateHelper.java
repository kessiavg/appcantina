package psj.com.br.lanches.helper;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kessia on 09/08/2017.
 */

public class DateHelper {

    public static String getDataFormatada(Date data) {
        if (data == null) {
            return null;
        }

        DateFormat format = new SimpleDateFormat("dd/MM/yy");
        return format.format(data);
    }

    public static Date convertStringToDate(String dateString) {
        Log.d("DATA ", dateString);
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy");
        try {
            return formato.parse(dateString);
        } catch (ParseException e){
            Log.d("DATA ", "CAIU NA EXCEÇÃO");
            return new Date();
        }
    }
}
