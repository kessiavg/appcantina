package psj.com.br.lanches;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import psj.com.br.lanches.helper.NumberHelper;
import psj.com.br.lanches.model.Lancamento;

/**
 * Created by kessia on 10/08/2017.
 */

public class LancamentosListAdapter extends ArrayAdapter<Lancamento> {
    private Context context;
    int resource;

    public LancamentosListAdapter(Context context, int resource, ArrayList<Lancamento> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String nome = getItem(position).getNomeCliente();
        Double valor = getItem(position).getValor();
        Boolean pago = getItem(position).getPago();

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource, parent, false);

        TextView tvNome = (TextView) convertView.findViewById(R.id.textViewNome);
        TextView tvValor = (TextView) convertView.findViewById(R.id.textViewValor);
        TextView tvPago = (TextView) convertView.findViewById(R.id.textViewPago);
        Button btnEditar = (Button) convertView.findViewById(R.id.botaoEditar);
        Button btnPagar = (Button) convertView.findViewById(R.id.botaoPagar);

        String valorString = NumberHelper.converterDoubleParaStringMonetario(valor);

        tvNome.setText(nome);
        tvValor.setText(valorString);

        if (pago){
            tvPago.setText("Pago");
            tvPago.setTextColor(0xFF119C2C);
            tvValor.setTextColor(0xFF119C2C);
            btnPagar.setVisibility(View.INVISIBLE);
        } else {
            tvPago.setText("Pendente");
            tvPago.setTextColor(Color.RED);
            tvValor.setTextColor(Color.RED);
        }
        if (getItem(position).getCantina().isCantinaEncerrada()) {
            btnEditar.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }



}
