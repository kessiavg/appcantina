package psj.com.br.lanches;

import android.os.Bundle;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;

import java.text.DecimalFormat;

import psj.com.br.lanches.dao.ConfiguracaoFirebase;
import psj.com.br.lanches.helper.Mask;
import psj.com.br.lanches.model.Cantina;
import psj.com.br.lanches.model.Lancamento;

public class InserirLancamentoActivity extends AuxActivity {

    public static boolean modeInserir;
    public static String nome;
    public static Double valor;
    public static String idLancamento;

    private EditText edtNomeCliente;
    private EditText edtValor;
    private Button btnCancelar;
    private Button btnSalvar;
    private static Cantina cantinaSession;
    DatabaseReference referenciaFirebase = ConfiguracaoFirebase.getFirebase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserir_lancamento);

        edtNomeCliente = (EditText) findViewById(R.id.edtNomeCliente);
        edtValor = (EditText) findViewById(R.id.edtValor);
        TextWatcher valorMask = Mask.monetario(edtValor);
        edtValor.addTextChangedListener(valorMask);

        if (!modeInserir){
            edtNomeCliente.setText(nome);

            DecimalFormat df = new DecimalFormat("#.00");
            String valorFormatado = df.format(valor);
            edtValor.setText(valorFormatado);
        }

        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modeInserir) {
                    gravarLancamento(edtValor.getText().toString(), edtNomeCliente.getText().toString());
                } else {
                    editarLancamento(edtValor.getText().toString(), edtNomeCliente.getText().toString());
                }

                abrirTelaVisualizarLancamentos(InserirLancamentoActivity.this);
            }
        });

        btnCancelar = (Button) findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaVisualizarLancamentos(InserirLancamentoActivity.this);
            }
        });
    }


    private void editarLancamento(String valor, String nome) {
        Lancamento l = new Lancamento();
        valor = valor.replace(",", ".");

        l.setPago(false);
        l.setNomeCliente(nome);
        l.setValor(Double.parseDouble(valor));
        l.setIdCantina(cantinaSession.getKey());

        referenciaFirebase.child("lancamento").child(idLancamento).setValue(l);

        // referenciaFirebase.child("cantina").child(cantinaSession.getKey()).child("lancamentos").child(ref.getKey()).setValue(l);
    }

    private void gravarLancamento(String valor, String nome) {
        Lancamento l = new Lancamento();
        valor = valor.replace(",", ".");

        l.setPago(false);
        l.setNomeCliente(nome);
        l.setValor(Double.parseDouble(valor));
        l.setIdCantina(cantinaSession.getKey());

        DatabaseReference ref = referenciaFirebase.child("lancamento").push();
        l.setIdLancamento(ref.getKey());
        ref.setValue(l);

       // referenciaFirebase.child("cantina").child(cantinaSession.getKey()).child("lancamentos").child(ref.getKey()).setValue(l);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {
        case R.id.iniciar:
            abrirTelaInserir(InserirLancamentoActivity.this);
            return true;
        case R.id.consultar:
            abrirTelaConsultar(InserirLancamentoActivity.this);
            return true;
        case R.id.relatorio:
            abrirTelaRelatorio(InserirLancamentoActivity.this);
            return true;
        case R.id.sair:
            finish();
            return true;
    }
        return super.onOptionsItemSelected(item);
    }

    public static void setarHash(Cantina c){
        cantinaSession = c;
    }
}
