package psj.com.br.lanches;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import psj.com.br.lanches.helper.DateHelper;
import psj.com.br.lanches.helper.NumberHelper;
import psj.com.br.lanches.model.Cantina;
import psj.com.br.lanches.model.Lancamento;

public class VisualizarLancamentosActivity extends AuxActivity {

    private static Cantina cantinaSession;
    private Double valorTotalPendente = new Double(0.0);

    ListView listView;
    TextView textViewTotalPendente;
    TextView textViewTotalPago;
    TextView textViewInfoPendencias;
    TextView textViewEmptyMessage;
    Button btnEditar;
    ArrayList<Lancamento> lancamentos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_lancamentos);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listViewLancamentos);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Lancamento l = getLancamentos().get((int) id);
                chamarConfirmacao(l);
            }
        });

        btnEditar = (Button) findViewById(R.id.botaoEditar);

        textViewTotalPendente = (TextView) findViewById(R.id.totalPendente);
        textViewTotalPago = (TextView) findViewById(R.id.totalPago);
        textViewInfoPendencias = (TextView) findViewById(R.id.infoPendencias);
        textViewInfoPendencias.setText(DateHelper.getDataFormatada(cantinaSession.getDate()) + " - " + cantinaSession.getLanche());
        textViewEmptyMessage = (TextView) findViewById(R.id.emptyMessageLancamentos);
        textViewEmptyMessage.setVisibility(View.INVISIBLE);

        Query query = referenciaFirebase.child("lancamento").orderByChild("idCantina").equalTo(cantinaSession.getKey());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator<DataSnapshot> list = dataSnapshot.getChildren().iterator();

                while (list.hasNext()){
                    DataSnapshot item = list.next();
                    String idCantina = (String) item.child("idCantina").getValue();
                    String nome = (String) item.child("nomeCliente").getValue();
                    Object valor = item.child("valor").getValue();
                    Boolean pago = (Boolean) item.child("pago").getValue();

                    Double valorDouble = new Double(0.0);

                    if (valor instanceof Double) {
                        valorDouble = (Double) valor;
                    } else if (valor instanceof Long) {
                        valorDouble = Double.valueOf((Long)valor);
                    }

                    if (!pago) {
                        setValorTotalPendente(getValorTotalPendente() + valorDouble);
                    }

                    Lancamento lancamento = new Lancamento(idCantina, valorDouble, pago, nome);
                    lancamento.setIdLancamento(item.getKey());
                    lancamento.setValor(valorDouble);
                    lancamento.setCantina(cantinaSession);
                    getLancamentos().add(lancamento);

                    textViewTotalPendente.setText(NumberHelper.converterDoubleParaStringMonetario(getValorTotalPendente()));
                }
                setarLancamentos();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Se ocorrer um erro
            }
        });

        FloatingActionButton inserirItem = (FloatingActionButton) findViewById(R.id.inserirItem);
        inserirItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InserirLancamentoActivity.setarHash(cantinaSession);
                abrirTelaInserirLancamentos(VisualizarLancamentosActivity.this, true);
            }
        });


        FloatingActionButton btnEncerrar = (FloatingActionButton) findViewById(R.id.btnEncerrar);
            btnEncerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EncerrarCantinaActivity.setarHash(cantinaSession);
                abrirTelaEncerrar(VisualizarLancamentosActivity.this);
            }
        });

        FloatingActionButton btnGrafico = (FloatingActionButton) findViewById(R.id.btnGrafico);
        btnGrafico.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                setarValoresGrafico(getValorTotalPendente().floatValue(), cantinaSession.getTotalPago().floatValue());
                abrirTelaGrafico(VisualizarLancamentosActivity.this);
            }
        });

        if (cantinaSession.isCantinaEncerrada()){
            inserirItem.setVisibility(View.INVISIBLE);
            btnEncerrar.setVisibility(View.INVISIBLE);

            textViewTotalPago.setText(NumberHelper.converterDoubleParaStringMonetario(cantinaSession.getTotalPago()));;
        } else {
            btnGrafico.setVisibility(View.INVISIBLE);
        }
    }

    public void onClickEditar(View v){
        LinearLayout vwParentRow = (LinearLayout)v.getParent();

        TextView child = (TextView)vwParentRow.getChildAt(0);
        Button btnEditar = (Button)vwParentRow.getChildAt(2);
        btnEditar.setText(child.getText());
        btnEditar.setText("I've been clicked!");

        int id = listView.getPositionForView(v);
        Lancamento l = getLancamentos().get((int) id);

        abrirTelaInserirLancamentosEdicao(VisualizarLancamentosActivity.this, false, l.getNomeCliente(), l.getValor(), l.getIdLancamento(), cantinaSession);
        vwParentRow.refreshDrawableState();
    }

    public void onClickPagar(View v){
        LinearLayout vwParentRow = (LinearLayout)v.getParent();

        TextView child = (TextView)vwParentRow.getChildAt(0);

        Button btnPagar = (Button)vwParentRow.getChildAt(3);
        btnPagar.setText(child.getText());
        int id = listView.getPositionForView(v);
        Lancamento l = getLancamentos().get((int) id);
        chamarConfirmacao(l);

        btnPagar.setText("Pago!");
        vwParentRow.refreshDrawableState();
    }

    public void setarLancamentos(){
        if (getLancamentos().isEmpty()){
            textViewEmptyMessage.setVisibility(View.VISIBLE);
        }
        LancamentosListAdapter adapter = new LancamentosListAdapter(this, R.layout.adapter_view_layout_lanc, getLancamentos());
        listView.setAdapter(adapter);
    }

    private void setarValoresGrafico(float valorPendente, float valorPago) {
        InserirLancamentoActivity.setarHash(cantinaSession);

        float valorTotal = valorPago + valorPendente;

        GraficoActivity.setPercentPago(valorPago / valorTotal * 100);
        GraficoActivity.setPercentPendente(valorPendente / valorTotal * 100);
        GraficoActivity.setValorPago(valorPago);
        GraficoActivity.setValorPendente(valorPendente);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {
        case R.id.iniciar:
            abrirTelaInserir(VisualizarLancamentosActivity.this);
            return true;
        case R.id.consultar:
            abrirTelaConsultar(VisualizarLancamentosActivity.this);
            return true;
        case R.id.relatorio:
            abrirTelaRelatorio(VisualizarLancamentosActivity.this);
            return true;
        case R.id.sair:
            finish();
            System.exit(0);
            return true;
    }
        return(super.onOptionsItemSelected(item));
    }

    public static void setarCantinaSession(Cantina cantina){
        cantinaSession = cantina;
    }

    public ArrayList<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(ArrayList<Lancamento> lancamentos) {
        this.lancamentos = lancamentos;
    }


    public Double getValorTotalPendente() {
        return valorTotalPendente;
    }

    public void setValorTotalPendente(Double valorTotalPendente) {
        this.valorTotalPendente = valorTotalPendente;
    }

    public void chamarConfirmacao(final Lancamento l) {

        if (l.getPago()) {
            Toast.makeText(this, "Já está pago.", Toast.LENGTH_SHORT).show();
        } else {

            AlertDialog alert = new AlertDialog.Builder(VisualizarLancamentosActivity.this)
                    .setTitle("Pagar")
                    .setMessage("Confirma Pagamento?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            atualizarPagamento(l);
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }
    }


    private void atualizarValorPendente(Lancamento lanc) {

        referenciaFirebase.child("lancamento").child(lanc.getIdLancamento()).setValue(lanc);

        try {
            Cantina c = (Cantina) cantinaSession.clone();

            referenciaFirebase.child("cantina").child(cantinaSession.getKey()).setValue(c);
        } catch (Exception e){

        }

    }

    private void atualizarPagamento(Lancamento lanc) {
        lanc.setPago(true);
        referenciaFirebase.child("lancamento").child(lanc.getIdLancamento()).setValue(lanc);

        try {
            Cantina c = (Cantina) cantinaSession.clone();
            if (cantinaSession.getTotalPago() != null) {
                c.setTotalPago(cantinaSession.getTotalPago() + lanc.getValor());
            } else {
                c.setTotalPago(lanc.getValor());
            }
            cantinaSession.setTotalPago(c.getTotalPago());
            referenciaFirebase.child("cantina").child(cantinaSession.getKey()).setValue(c);
        } catch (Exception e){

        }

    }
}
