package psj.com.br.lanches.model;

/**
 * Created by kessia on 09/08/2017.
 */

public class Cliente {
    private String nome;
    private Long id;

    public Cliente(){
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
